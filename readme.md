<p style="text-align: center">
    <img alt="logo" src="https://gitlab.com/uploads/-/system/project/avatar/5152864/favicon.png?width=32">
</p>

<p></p>

## FITemailing SDK

Source Development KIT pro FITEmailing API [FITemailing.cz](https://fitemailing.cz)

Instalaci provedete: **composer require fitemailing/sdk**<br>

Vývojová verze: **composer require fitemailing/sdk:dev-master**