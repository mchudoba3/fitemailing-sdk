<?php

require_once __DIR__ . '/../src/Fitemailing/Exception/Exception.php';
require_once __DIR__ . '/../src/Fitemailing/Exception/IOException.php';
require_once __DIR__ . '/../src/Fitemailing/Exception/AuthException.php';
require_once __DIR__ . '/../src/Fitemailing/Exception/LogicalException.php';
require_once __DIR__ . '/../src/Fitemailing/IO/StreamInterface.php';
require_once __DIR__ . '/../src/Fitemailing/Helpers/Utils.php';
require_once __DIR__ . '/../src/Fitemailing/Services/Entity/Email.php';
require_once __DIR__ . '/../src/Fitemailing/Services/BaseService.php';
require_once __DIR__ . '/../src/Fitemailing/Services/EmailService.php';
require_once __DIR__ . '/../src/Fitemailing/Services/CrewService.php';
require_once __DIR__ . '/../src/Fitemailing/IO/Response.php';
require_once __DIR__ . '/../src/Fitemailing/IO/Request.php';
require_once __DIR__ . '/../src/Fitemailing/IO/HttpStream.php';
require_once __DIR__ . '/../src/Fitemailing/Auth/Authenticator.php';
require_once __DIR__ . '/../src/Fitemailing/Client.php';