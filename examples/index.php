<?php

if ($session_id = @file_get_contents('SESSION_ID.txt')) {
	session_id($session_id);
	session_start();
} else {
	session_start();
	file_put_contents('SESSION_ID.txt', session_id());
}

require_once __DIR__ . '/loader.php';

$token = $_SESSION['token'] ?? null;

$client = new \Fitemailing\Client('http://api.newsletters.dev/api');

function printEmails(stdClass $data): void
{
    foreach ($data->data as $email) {
        echo $email->email . PHP_EOL;
    }
}

try {
	if ($token === null) {
		$client->login('root', 'eqwecwqewqcwqeqwqw');
		$_SESSION['token'] = $client->getAccessToken();
	} else {
		$client->setAccessToken($token);
	}

	$emailService = $client->getEmailService();

	$data = $emailService->getEmail(10);
	echo $data->data->email . PHP_EOL;
    foreach ($data->data->crews->data as $crew) {
        echo $crew->name . '(' . $crew->activated . ')' . PHP_EOL;
    }

	$emails = $emailService->getEmails(); // page 1
    printEmails($emails);

    $emails = $emailService->getEmails(2); // page 2
    printEmails($emails);

	/**
	 * Create Email
	 */
	$emailEntity = new \Fitemailing\Services\Entity\Email();
	$emailEntity->setEmail('test111@example.com');
	$emailEntity->addCrew([
		'crewID'	=> 1,
		'activated'	=> 0
	]);
	try {
		$response = $emailService->createEmail($emailEntity);
		echo $response->status . PHP_EOL;
	} catch (\Fitemailing\Exception\LogicalException $exception) {
		echo $exception->getMessage() . PHP_EOL;
	}

	/** Get All Crews */
	$crewService = $client->getCrewService();
	$crews = $crewService->getCrews();
    foreach ($crews->data as $crew) {
        echo $crew->name . '(' . $crew->activated . ')' . PHP_EOL;
	}
	echo 'Total crews: ' . $crews->meta->pagination->total . PHP_EOL;

} catch (\Fitemailing\Exception\AuthException $exception) {
	echo $exception->getMessage() . PHP_EOL;
	session_unset();
} catch (\Fitemailing\Exception\IOException|\Fitemailing\Exception\LogicalException|\Fitemailing\Exception\Exception $exception) {
	echo $exception->getMessage() . PHP_EOL;
}

session_write_close();