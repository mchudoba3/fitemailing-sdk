<?php declare(strict_types=1);

namespace Fitemailing;

use Fitemailing\Auth\Authenticator;
use Fitemailing\IO\HttpStream;
use Fitemailing\Services\CrewService;
use Fitemailing\Services\EmailService;

final class Client
{
    private HttpStream $httpStream;
    private string $baseUrl;
    private string $accessToken = '';
    /** @var array<\Fitemailing\Services\BaseService> */
    private array $services = [];

    public function __construct(string $baseUrl)
    {
        $this->httpStream = new HttpStream();
        $this->baseUrl = $baseUrl;
    }

    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    public function login(string $login, string $apiKey): void
    {
        /** @var Authenticator $authenticator */
        $authenticator = $this->getService(Authenticator::class);
        $this->accessToken = $authenticator->login($login, $apiKey);
    }

    public function getEmailService(): EmailService
    {
        /** @var EmailService $emailService */
        $emailService = $this->getService(EmailService::class);
        $emailService->setAccessToken($this->accessToken);

        return $emailService;
    }

    public function getCrewService(): CrewService
    {
        /** @var CrewService $crewService */
        $crewService = $this->getService(CrewService::class);
        $crewService->setAccessToken($this->accessToken);

        return $crewService;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    private function getService(string $name): mixed
    {
        foreach ($this->services as $service) {
            if ($service instanceof $name) {
                return $service;
            }
        }

        return new $name($this->baseUrl, $this->httpStream);
    }
}