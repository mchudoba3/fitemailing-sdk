<?php declare(strict_types=1);

namespace Fitemailing\Exception;

class AuthException extends Exception
{
}