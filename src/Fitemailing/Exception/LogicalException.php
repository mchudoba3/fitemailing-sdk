<?php declare(strict_types=1);

namespace Fitemailing\Exception;

class LogicalException extends Exception {
}