<?php declare(strict_types=1);

namespace Fitemailing\Exception;

class IOException extends Exception {
}