<?php declare(strict_types=1);

namespace Fitemailing\Helpers;

class Utils
{
    /**
     * @param array<string,string> $arr
     * @return array<string,string>
     */
    public static function normalize(array $arr): array
    {
        $normalized = [];
        foreach ($arr as $key => $val) {
            $normalized[strtolower($key)] = $val;
        }

        return $normalized;
    }
}