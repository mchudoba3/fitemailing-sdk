<?php declare(strict_types=1);

namespace Fitemailing\Auth;

use Fitemailing\IO\Request;
use Fitemailing\Services\BaseService;

class Authenticator extends BaseService
{
    /**
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \JsonException
     */
    public function login(string $login, string $apiKey): string
    {
        $request = new Request($this->baseUrl . '/authenticate', 'POST', ['login' => $login, 'key' => $apiKey]);

        return $this->process($request)->token;
    }
}