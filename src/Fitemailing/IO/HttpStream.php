<?php declare(strict_types=1);

namespace Fitemailing\IO;

use Fitemailing\Exception\IOException;

class HttpStream implements StreamInterface
{
    protected const FORM_URLENCODED = 'application/x-www-form-urlencoded';

    /** @var array|null[] */
    private static array $ENTITY_HTTP_METHODS = ['POST' => null, 'PUT' => null];
    /** @var array|int[] */
    private static array $DEFAULT_HTTP_CONTEXT = ['follow_location' => 0, 'ignore_errors' => 1];
    /** @var array|bool[] */
    private static array $DEFAULT_SSL_CONTEXT = ['verify_peer' => true];


    /**
     * @throws \Fitemailing\Exception\IOException
     */
    public function send(Request $request): Response
    {
        $default_options = stream_context_get_options(stream_context_get_default());
        $request->setRequestHeaders(['Cache-Control' => 'no-cache']);

        $requestHttpContext = array_key_exists('http', $default_options) ? $default_options['http'] : [];
        if (array_key_exists($request->getRequestMethod(), self::$ENTITY_HTTP_METHODS)) {
            $request = $this->processEntityRequest($request);
        }

        if ($request->getPostBody()) {
            $requestHttpContext['content'] = $request->getPostBody();
        }

        $requestHeaders = $request->getRequestHeaders();
        if ($requestHeaders) {
            $headers = '';
            foreach ($requestHeaders as $k => $v) {
                $headers .= "$k: $v\r\n";
            }
            $requestHttpContext['header'] = $headers;
        }

        $requestHttpContext['method'] = $request->getRequestMethod();
        $requestHttpContext['user_agent'] = $request->getUserAgent();
        $requestHttpContext['protocol_version'] = '1.1';

        $requestSslContext = array_key_exists('ssl', $default_options) ? $default_options['ssl'] : [];

        $context = stream_context_create([
            'http' => array_merge(self::$DEFAULT_HTTP_CONTEXT, $requestHttpContext),
            'ssl'  => array_merge(self::$DEFAULT_SSL_CONTEXT, $requestSslContext)
        ]);

        $response_data = file_get_contents($request->getUrl(), false, $context);

        if (false === $response_data) {
            throw new IOException('HTTP Error: Unable to connect');
        }

        return new Response($http_response_header, $response_data);
    }

    protected function processEntityRequest(Request $request): Request
    {
        $postBody = $request->getPostBody();
        $contentType = $request->getRequestHeader('content-type');

        // Set the default content-type as application/x-www-form-urlencoded.
        if (null === $contentType) {
            $contentType = self::FORM_URLENCODED;
            $request->setRequestHeaders(['content-type' => $contentType]);
        }

        // Force the payload to match the content-type asserted in the header.
        if ($contentType === self::FORM_URLENCODED && is_array($postBody)) {
            $postBody = http_build_query($postBody);
            $request->setPostBody($postBody);
        }

        // Make sure the content-length header is set.
        if (!$postBody && is_string($postBody)) {
            $postsLength = strlen($postBody);
            $request->setRequestHeaders(['content-length' => (string)$postsLength]);
        }

        return $request;
    }
}