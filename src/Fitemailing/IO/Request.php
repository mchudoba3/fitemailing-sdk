<?php declare(strict_types=1);

namespace Fitemailing\IO;

use Fitemailing\Helpers\Utils;

class Request
{
    private string $url;
    private string $requestMethod;
    /** @var array<string,string>|string|string[]|null  */
    private array|string|null $postBody;
    /** @var array<string> */
    private array $requestHeaders = [];

    /**
     * @param array<string,string>|string|null $postBody
     */
    public function __construct(string $url, string $method = 'GET', array|string $postBody = null)
    {
        $this->url = $url;
        $this->requestMethod = $method;
        $this->postBody = $postBody;
    }

    /**
     * @return array<string>
     */
    public function getQueryParams(): array
    {
        if ($pos = strpos($this->url, '?')) {
            $queryStr = substr($this->url, $pos + 1);
            $params = [];
            parse_str($queryStr, $params);

            return $params;
        }

        return [];
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array<string,string>|string|null
     */
    public function getPostBody(): array|string|null
    {
        return $this->postBody;
    }

    /**
     * @return string[]
     */
    public function getRequestHeaders(): array
    {
        return $this->requestHeaders;
    }

    public function getRequestMethod(): string
    {
        return $this->requestMethod;
    }

    public function getUserAgent(): string
    {
        return 'API';
    }

    /**
     * @param array<string,string> $requestHeaders
     */
    public function setRequestHeaders(array $requestHeaders): void
    {
        $requestHeaders = Utils::normalize($requestHeaders);
        if ($this->requestHeaders) {
            $requestHeaders = array_merge($this->requestHeaders, $requestHeaders);
        }
        $this->requestHeaders = $requestHeaders;
    }

    public function getRequestHeader(string $key): ?string
    {
        return $this->requestHeaders[$key] ?? null;
    }

    /**
     * @param array<string,string>|string|null $postBody
     */
    public function setPostBody(array|string $postBody = null): void
    {
        $this->postBody = $postBody;
    }
}