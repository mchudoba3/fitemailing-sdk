<?php declare(strict_types=1);

namespace Fitemailing\IO;

interface StreamInterface {
	public function send(Request $request):Response;
}