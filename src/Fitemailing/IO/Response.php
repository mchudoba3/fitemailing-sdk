<?php declare(strict_types=1);

namespace Fitemailing\IO;

class Response
{
    public const UNKNOWN = 'UNKNOWN';
    /** @var array<string,string> */
    protected array $responseHeaders;
    protected string $responseHttpCode;
    protected string $responseBody;

    /**
     * @param array<string> $responseHeaders
     */
    public function __construct(array $responseHeaders, string $responseBody)
    {
        $this->responseHttpCode = $this->getHttpResponseCode($responseHeaders);
        $this->responseHeaders = $this->getHttpResponseHeaders($responseHeaders);
        $this->responseBody = $responseBody;
    }

    public function getResponseHttpCode(): string
    {
        return $this->responseHttpCode;
    }

    /**
     * @return array<string>
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    public function getResponseBody(): string
    {
        return $this->responseBody;
    }

    /**
     * @param array<string> $response_headers
     */
    private function getHttpResponseCode(array $response_headers): string
    {
        foreach ($response_headers as $header) {
            if (strncasecmp('HTTP', $header, strlen('HTTP')) === 0) {
                $response = explode(' ', $header);

                return $response[1];
            }
        }

        return self::UNKNOWN;
    }

    /**
     * @param array<string> $response_headers
     * @return array<string,string>
     */
    private function getHttpResponseHeaders(array $response_headers): array
    {
        $headers = [];

        foreach ($response_headers as $header) {
            $header_parts = explode(':', $header);
            if (count($header_parts) === 2) {
                $headers[$header_parts[0]] = $header_parts[1];
            }
        }

        return $headers;
    }
}