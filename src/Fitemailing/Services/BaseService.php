<?php declare(strict_types=1);

namespace Fitemailing\Services;

use Fitemailing\Exception\AuthException;
use Fitemailing\IO\HttpStream;
use Fitemailing\IO\Request;
use Fitemailing\Exception\IOException;
use Fitemailing\Exception\LogicalException;

abstract class BaseService
{
    protected HttpStream $httpStream;
    protected string $baseUrl;
    protected string $accessToken = '';

    public function __construct(string $baseUrl, HttpStream $httpStream)
    {
        $this->baseUrl = $baseUrl;
        $this->httpStream = $httpStream;
    }

    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    protected function process(Request $request): \stdClass
    {
        $response = $this->httpStream->send($request);
        $code = $response->getResponseHttpCode();

        switch ($code) {
            case '200':
                $data = json_decode($response->getResponseBody(), false, 512, JSON_THROW_ON_ERROR);
                if (isset($data->error)) {
                    throw new LogicalException($data->error);
                }
                if (! $data instanceof \stdClass) {
                    throw new LogicalException('Wrong response type. Expected stdClass but ' . get_debug_type($data) . ' given.');
                }

                return $data;
            case '401':
                $data = json_decode($response->getResponseBody(), false, 512, JSON_THROW_ON_ERROR);
                throw new AuthException($data instanceof \stdClass ? $data->message : 'Authentication error!', (int)$code);
            default:
                throw new IOException('Error ' . $code);
        }
    }
}