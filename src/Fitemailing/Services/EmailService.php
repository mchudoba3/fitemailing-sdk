<?php declare(strict_types=1);

namespace Fitemailing\Services;

use Fitemailing\IO\Request;
use Fitemailing\Services\Entity\Email;

class EmailService extends BaseService
{
    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    public function getEmail(int $emailId): \stdClass
    {
        $request = new Request($this->baseUrl . '/email/' . $emailId, 'GET');
        $request->setRequestHeaders(['Authorization' => 'Bearer ' . $this->accessToken]);

        return $this->process($request);
    }

    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    public function getEmails(int $page = 1): \stdClass
    {
        $request = new Request($this->baseUrl . '/emails?page=' . $page, 'GET');
        $request->setRequestHeaders(['Authorization' => 'Bearer ' . $this->accessToken]);

        return $this->process($request);
    }

    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    public function createEmail(Email $email): \stdClass
    {
        $request = new Request($this->baseUrl . '/email', 'POST', (string)$email);
        $request->setRequestHeaders(['Authorization' => 'Bearer ' . $this->accessToken]);
        $request->setRequestHeaders(['Content-Type' => 'application/json']);

        return $this->process($request);
    }
}