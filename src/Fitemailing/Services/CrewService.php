<?php declare(strict_types=1);

namespace Fitemailing\Services;

use Fitemailing\IO\Request;

class CrewService extends BaseService
{
    /**
     * @throws \Fitemailing\Exception\AuthException
     * @throws \Fitemailing\Exception\IOException
     * @throws \Fitemailing\Exception\LogicalException
     * @throws \JsonException
     */
    public function getCrews(int $page = 1): \stdClass
    {
        $request = new Request($this->baseUrl . '/crews?page=' . $page, 'GET');
        $request->setRequestHeaders(['Authorization' => 'Bearer ' . $this->accessToken]);

        return $this->process($request);
    }
}