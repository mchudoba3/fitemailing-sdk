<?php declare(strict_types=1);

namespace Fitemailing\Services\Entity;

final class Email
{
    protected string $email = '';
    /** @var array<array<string,int>> */
    protected array $crews = [];

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return array<array<string,int>>
     */
    public function getCrews(): array
    {
        return $this->crews;
    }

    /**
     * @param array<string,int> $crew
     */
    public function addCrew(array $crew): void
    {
        $this->crews[] = $crew;
    }

    /**
     * @throws \JsonException
     */
    public function __toString(): string
    {
        $entity = [
            'email' => $this->getEmail(),
            'crews' => $this->getCrews()
        ];

        return json_encode($entity, JSON_THROW_ON_ERROR);
    }
}